package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.AnnotationAction;
import com.rostyslavprotsiv.model.action.ClassParser;
import com.rostyslavprotsiv.model.entity.MyClass;
import com.rostyslavprotsiv.view.Menu;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class Controller {
    private static final AnnotationAction ANNOTATION_ACTION
            = new AnnotationAction();
    private static final Menu MENU = new Menu();

    public void execute() {
        MyClass myClass = new MyClass(1, "aaa", 1.0);
        String objectBeforeChanging = myClass.toString();
        Field[] annotationTestResult = ANNOTATION_ACTION.getAnnotatedFields();
        double[] annotationValues = ANNOTATION_ACTION.getAnnotationValues();
        MENU.welcome();
        MENU.outTestAnnotation(Arrays.toString(annotationTestResult));
        MENU.outAnnotationValues(Arrays.toString(annotationValues));
        MENU.outThreeMethodsInvocation();
        try {
            ANNOTATION_ACTION.invokeThreeMethods();
            ANNOTATION_ACTION.setValue(myClass);
            MENU.outResultOfValueSetting(objectBeforeChanging, myClass.toString());
            MENU.outTwoMethodsInvocation();
            ANNOTATION_ACTION.invokeTwoMethods();
            MENU.outClassParsingExample(ClassParser.getInfo(myClass));
        } catch (NoSuchMethodException | InvocationTargetException
                | IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
