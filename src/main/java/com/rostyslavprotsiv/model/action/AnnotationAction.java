package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.MyAnnotation;
import com.rostyslavprotsiv.model.entity.MyClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

public class AnnotationAction {
    private static final Logger LOGGER = LogManager.getLogger(
            AnnotationAction.class);

    public Field[] getAnnotatedFields() {
//        MyClass myClass = new MyClass(1, "aaa", 1.0);
//        MyClass myClass1 = new MyClass(2, "bbb", 2.0);
//        MyClass myClass2 = new MyClass(3, "ccc", 3.0);
        List<Field> fields = new LinkedList<>();
        for (Field f : MyClass.class.getDeclaredFields()) {
            if (f.isAnnotationPresent(MyAnnotation.class)) {
                fields.add(f);
//                System.out.println("Type : " + f.getType());
//                System.out.println("Name : " + f.getName());
//                f.setAccessible(true);
//                System.out.println("Value : " + f.getInt(myClass1));
            }
        }
        return fields.toArray(new Field[0]);
    }

    public double[] getAnnotationValues() {
        int length = MyClass.class.getDeclaredFields().length;
        Field[] fields = MyClass.class.getDeclaredFields();
        double[] values = new double[length];
        MyAnnotation myAnnotation;
        for (int i = 0; i < length; i++) {
            if (fields[i].isAnnotationPresent(MyAnnotation.class)) {
                myAnnotation = fields[i].getAnnotation(MyAnnotation.class);
                values[i] = myAnnotation.someValue();
            }
        }
        return values;
    }

    public void invokeThreeMethods() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        MyClass myClass = new MyClass(1, "aaa", 1.0);
        MyClass myClass1 = new MyClass(2, "bbb", 2.0);
        Method method1 = MyClass.class.getMethod("method1");
        Method method2 = MyClass.class.getDeclaredMethod("method2",
                int.class, int.class);
        Method method3 = MyClass.class.getMethod("method3", Object[].class);
        method2.setAccessible(true);
        method1.invoke(myClass);
        LOGGER.info(method2.invoke(myClass1, 2, 3));
        LOGGER.info(method3.invoke(myClass, new Object[] {
                new String[] {"aa", "bb"}}));
    }

    public void setValue(MyClass myClass) throws NoSuchFieldException,
            IllegalAccessException {
        Field field = MyClass.class.getDeclaredField("id");
        field.setAccessible(true);
        field.set(myClass, 34);
    }

    public void invokeTwoMethods() throws NoSuchMethodException,
                            InvocationTargetException, IllegalAccessException {
        MyClass myClass = new MyClass(1, "aaa", 1.0);
        Method myMethod1 = MyClass.class.getMethod("myMethod",
                String.class, int[].class);
        Method myMethod2 = MyClass.class.getMethod("myMethod",
                String[].class);
        myMethod1.invoke(myClass, "WOW", new int[]{1, 3, 5, 6});
        myMethod2.invoke(myClass, new Object[] {
                new String[] {"ff", "ggh", "huj"}});
    }
}
