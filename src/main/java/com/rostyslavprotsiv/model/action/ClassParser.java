package com.rostyslavprotsiv.model.action;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class ClassParser {
    public static <T> String getInfo(T object) throws IllegalAccessException {
        StringBuilder result = new StringBuilder();
        Class<? extends Object> clazz = object.getClass();
        result.append(Modifier.toString(clazz.getModifiers()) + " ");
        result.append("class ");
        result.append(clazz.getSimpleName() + " {\n");
        setFields(object, result);
        result.append("\n");
        setConstructors(object, result);
        setMethods(object, result);
        result.append("}\n");
        return result.toString();
    }

    private static <T> void setFields(T object, StringBuilder result)
            throws IllegalAccessException {
        Class<? extends Object> clazz = object.getClass();
        for (Field f : clazz.getDeclaredFields()) {
            setAnnotations(result, f);
            result.append("\t" + Modifier.toString(f.getModifiers()) + " ");
            result.append(f.getType().getSimpleName() + " ");
            result.append(f.getName() + " = ");
            f.setAccessible(true);
            result.append(f.get(object) + ";\n");
        }
    }

    private static <T> void setConstructors(T object, StringBuilder result) {
        Class<? extends Object> clazz = object.getClass();
        for (Constructor c : clazz.getDeclaredConstructors()) {
            setAnnotations(result, c);
            result.append("\t" + Modifier.toString(c.getModifiers()) + " ");
            result.append(c.getName().replaceAll("^.*\\.", "") + "(");
            setParameters(result, c);
            result.append(")" + ";\n");
        }
    }

    private static <T> void setMethods(T object, StringBuilder result) {
        Class<? extends Object> clazz = object.getClass();
        for (Method m : clazz.getDeclaredMethods()) {
            setAnnotations(result, m);
            result.append("\t" + Modifier.toString(m.getModifiers()) + " ");
            result.append(m.getReturnType().getSimpleName() + " ");
            result.append(m.getName().replaceAll("^.*\\.", "") + "(");
            setParameters(result, m);
            result.append(")" + ";\n");
        }
    }

    private static void setParameters(StringBuilder result,
                                           Executable executable) {
        Parameter[] params = executable.getParameters();
        if (params.length != 0) {
            for (int i = 0; i < params.length - 1; i++) {
                result.append(params[i].getType().getSimpleName() + " ");
                result.append(params[i].getName() + ", ");
            }
            result.append(params[params.length - 1].getType()
                    .getSimpleName() + " ");
            result.append(params[params.length - 1].getName());
        }
    }

    private static void setAnnotations(StringBuilder result,
                                           AccessibleObject accessibleObject) {
        Annotation[] annotations;
        annotations = accessibleObject.getDeclaredAnnotations();
        if (annotations.length != 0) {
            for (Annotation a : annotations) {
                result.append("\t@" + a.annotationType().getSimpleName()
                        + a.toString()
                        .replaceAll("^@[^\\(]*", "") + "\n");
            }
        }
    }
}
