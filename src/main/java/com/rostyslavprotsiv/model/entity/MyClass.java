package com.rostyslavprotsiv.model.entity;

import java.util.Arrays;

public class MyClass {
    @MyAnnotation(someValue = 45.1)
    private int id;
    @MyAnnotation
    public String name;
    private double age;

    public MyClass(int id, String name, double age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    @MyAnnotation(someValue = 14)
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public void method1() {
        System.out.println("Method1 invocation");
    }

    private String method2(int a, int b) {
        return String.valueOf(a + b + id);
    }

    public <T> T method3(T ... params) {
        return params[0];
    }

    public void myMethod(String a, int ... args) {
        System.out.println("a = " + a + " args: " + Arrays.toString(args));
    }

    public boolean myMethod(String ... args) {
        System.out.println(" args: " + Arrays.toString(args));
        return true;
    }

    @Override
    public String toString() {
        return "MyClass{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
