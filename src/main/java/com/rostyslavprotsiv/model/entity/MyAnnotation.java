package com.rostyslavprotsiv.model.entity;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
//@Target(ElementType.FIELD)
@Documented
@Inherited
public @interface MyAnnotation {
    double someValue() default 35.1;
}
