package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public void welcome() {
        LOGGER.info("Hi, welcome to my software!");
    }

    public void outTestAnnotation(String fields) {
        LOGGER.info("The next fields are "
                + "annotated with my own annotation :" + fields);
    }

    public void outAnnotationValues(String values) {
        LOGGER.info("Annotation values:" + values);
    }

    public void outThreeMethodsInvocation() {
        LOGGER.info("The invocation of three methods:");
    }

    public void outResultOfValueSetting(String objectBefore,
                                        String objectAfter) {
        LOGGER.info("The object before setting value into field:"
                + objectBefore);
        LOGGER.info("The object after setting value into field:"
                + objectAfter);
    }

    public void outTwoMethodsInvocation() {
        LOGGER.info("The invocation of two methods:");
    }

    public void outClassParsingExample(String parsed) {
        LOGGER.info("The example of MyClass parsing: \n" + parsed);
    }
}
